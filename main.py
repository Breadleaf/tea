#!/usr/bin/python3

from os.path import exists
from sys import argv

if __name__ == "__main__":
    # Check for help flag
    if "-h" in argv or "--help" in argv:
        print(f"Usage: {argv[0]} [OPTION]... [FILE]...")
        print("Copy standard input to each FILE, and also to standard output.\n")
        print("  -a, --append              append to the given FILEs, do not overwrite\n")
        print("Please note that this program is not meant to replace `tee` gnu util.")
        print("This program should not be used in production environments as it is")
        print("only meant to be a small challenge for me to replicate gnu `tee`")
        print("as fast as I can.")
        print("For my other projects visit: https://gitlab.com/breadleaf")
        exit(0)

    # Begin main code section
    try:
        # If no file name is provided - write to stdout
        if len(argv) < 2:
            while True:
                print(input())

        # Write to each file given to the program
        else:
            mode = "w"

            # Check for append flag
            if "-a" in argv:
                mode = "r"
                argv.remove("-a")
            if "--append" in argv:
                mode = "r"
                argv.remove("--append")

            # Begin write loop to files
            files = [open(x, mode) for x in argv[1:]]
            while True:
                text = input()
                [f.write(text + "\n") for f in files]
                print(text)

    # This is used for if a string is piped into this program. For example:
    # echo "Hello World!" | tea
    # The above will produce the following error hence why I catch it.
    except EOFError:
        exit(0)

    # This is more obvious. I added it to closely mimic the `tee` gnu util.
    except KeyboardInterrupt:
        print()
        exit(0)
