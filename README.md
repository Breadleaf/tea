# TEA

`tea` is a (not 1:1) recreation of gnu's `tee` util. `tea` is not designed with intent of replacing `tee` nor is it designed to be used in any production / official uses. `tea` was meant to be a challenge for myself to re-write yet another gnu util as fast as I can.

# Examples of use

```
$ echo "Hello World" | tea a b c
Hello World

$ cat a
Hello World

$ cat b
Hello World

$ cat c
Hello World

$ tea
Hello
Hello
World
World
^C

$ tea d
Hello
^C

$ cat d
Hello

$ tea -a a
!
!
^C

$ cat a
Hello World!
```
